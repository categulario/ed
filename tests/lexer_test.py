from multipub.lexer import Lexer


def test_simple():
    file = '''
'''

    lexer = Lexer()
    tokens = lexer.test(file)

    assert tokens == ['some', 'token']
