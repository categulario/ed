.PHONY: pytest lint test

test: pytest lint

pytest:
	pytest -xvv

lint:
	flake8 --ignore E501 multipub/ tests/
