# Esto es para hacer un archivo para test con el manual

import re

man = open('man.ed').read()

# Para el test se quitan todos los bloques verbatim,
# esto induce a que el parser considere las estructuras
man = re.sub(r'[^\n]+; +DEL[^\n]*\n', '', man)
man = re.sub(r'\s*TODO: +BORRADOR(.|\n)+', '', man)

file = open('test.man.ed', 'w')
file.write(man)
file.close()
