# ED, un lenguaje de marcado para la publicación desde una sola fuente

## Filosofía del lenguaje

* Syntaxis mínima, concisa, sin tanto caracter.
* uso de comandos sencillos en terminal
* la invocación en terminal produce (por defecto) salida mínima y accionable,
  pero a petición de la usuaria se pueden pedir más niveles de información para
  depuración.

### Espectos a decidir

* Las constantes (si se llaman así, tengo entendido que sí te latió esa idea de reutilización de metadatos)
* Los scopes, tengo entendido que sí te latió la idea
* Includes (y si pueden ser como macros)
* Notas al pie
* Bibliografía
* Conversión de identificadores
* Renderizaciones
* Filtros

## Interfaz de línea de comandos

* `ed convert NAMESPACE ED` => uso de espacios de nombres en lugar ded nombres de formato para conversión
* `ed analize ED` => el tipo de análisis que hará como validación de sintaxis, de brackets, de quotes, de enlaces y de palabras
* `ed exec ED` => Lo de la programación letrada (ya no me gusta la traducción «literaria» es equívoca), que la neta no me parece tanto pedo, es solo con este comando la ejecución de los bloques de código y la implementación de los macros (que son bloques de código). Piensa esto para toy programs, como lo que me andas explicando ahorita o para documentación y nada más

## Desarrollo

Instala las dependencias, de preferencia dentro de un entorno virtual:

    pip install -r requirements.txt

Y corre las pruebas

    pytest

Hay 3 test:

1. `test.simple.ed`: prueba simple de la sintaxis.
2. `test.man.ed`: prueba completa sobre el manual de ED.
3. `test.snippet.txt`: prueba de `:include` que forma parte de la prueba completa.

La prueba completa se hace a partir del `man.ed` al cual
se le eliminan casi todos los bloques verbatim para que
la sintaxis sea aplicada.

Por ello, cada vez que se cambia el `man.ed` es necesario
reconstruir su test con:

```python
python make_test_man.py
```

TODO:

* Terminar el lexer para la sintaxis en línea.
* Hacer parsing.
* Hacer AST desde el parsing.
