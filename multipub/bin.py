import argparse
from argparse import FileType


def main():
    parser = argparse.ArgumentParser(description='Herramienta de publicación desde una sola fuente')

    subparsers = parser.add_subparsers(
        required=True, metavar='SUBCOMMAND',
    )

    check_parser = subparsers.add_parser('check', help='verifica la estructura de un documento sin compilarlo')
    check_parser.add_argument(
        'input_file', type=FileType('r'), metavar='INPUT',
        help='Input file',
    )

    convert_parser = subparsers.add_parser('convert', help='Convierte tu fuente a alguno de los formatos soportados')
    convert_parser.add_argument(
        'input_file', type=FileType('r'), metavar='INPUT',
        help='Input file',
    )
    convert_parser.add_argument(
        'output_file', type=FileType('w'), metavar='OUTPUT',
        help='Output file',
    )
    convert_parser.add_argument(
        '-f --format', metavar='FORMAT',
        help='Format to use when converting',
    )

    exec_parser = subparsers.add_parser('exec', help='Ejecuta tu documento como un script')
    exec_parser.add_argument(
        'input_file', type=FileType('r'), metavar='INPUT',
        help='Input file',
    )

    parser.parse_args()
