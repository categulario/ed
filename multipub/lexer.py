import codecs
import ply.yacc as yacc
import ply.lex as lex
from   ply.lex  import TOKEN


class Lexer(object):
    # Solo sé que se necesita cuando ply está en una clase
    # cfr. https://ply.readthedocs.io/en/latest/ply.html#alternative-specification-of-lexers
    def __init__(self):
        self.lexer = lex.lex(module=self)
        # Tipo de error y mensaje de error por defecto
        self.error_by_user = False
        self.error_msg = 'carácter inválido'

    # Nombre de los tokens que se utilizarán
    tokens = (
        'CMT', 'SYM', 'SHORTMODE', 'ARGSINI', 'ARGSEND',
        'ARRAYINI', 'ARRAYEND', 'KEY', 'STR', 'RAW',
        'FLOAT', 'INT', 'OPT', 'BOOL', 'SEP', 'GROUPINI',
        'GROUPEND', 'CUT', 'TXT',
    )

    # Obtiene texto circunscrito al carácter indicado
    # Basado en lineStringLiteral de Kotlin
    # cfr. https://kotlinlang.org/docs/reference/grammar.html#lineStringLiteral
    # TL;DR: permite que el texto circunscrito tenga
    # el carácter indicado si este está escapado
    # Ejemplo 1: "Una \"cadena\" con comillas escapadas"
    # Ejemplo 2: %Un \%verbatim\%%
    def str_re(char):
        inside = [ r'([^', r'\\]|\\(t|b|r|n|\\|', r'))*' ]
        return char + char.join(inside) + char

    # Atajos para @TOKEN
    # cfr. https://ply.readthedocs.io/en/latest/ply.html#the-token-decorator

    # Separador de símbolos o parámetros
    e = r':' # Separador
    s = r'[^\S\n]*' # Cero o más espacios sin salto de línea

    # Cadenas de texto
    raw = str_re(r'\'') # 'Cadena cruda'
    str = str_re(r'"') # "Cadena"

    # Números
    # Basado en IntegerLiteral de Kotlin
    # cfr. https://kotlinlang.org/docs/reference/grammar.html#IntegerLiteral
    # TL;DR: permite usar guion bajo como separador
    i = r'([1-9][\d_]*\d|\d)' # Número entero
    i0 = i + r'*' # Cero o más números enteros
    i1 = i + r'+' # Uno o más números enteros

    # Definición de tokens

    # ! Las def han de preservar el orden de declaración
    # cfr. https://ply.readthedocs.io/en/latest/ply.html#specification-of-tokens
    #      https://stackoverflow.com/a/73364679
    # TL;DR:
    # 1. Descarta los tokens especificados en 't_ignore'
    # 2. Aplica los tokens definidos como funciones según como
    #    aparecen en este archivo
    # 3. Aplica los token definidos como variable empezando
    #    por la expresión regular más larga
    # 4. Aplica los tokens especificados en 'literals'
    # 5. Llama a 't_error' si nada de lo anterior funcionó

    # Comentarios
    def t_CMT(self, t):
        r';.*\n'
        return self.fix_lineno(t) # Por el verbatim se queda

    # Símbolos para iniciar bloques
    valid_syms = """
    authors block_code block_quote block_verbatim bloque_cita
    bloque_codigo bloque_literal bloque_verbatim box caption
    ce celda celda_h cell cell_h center ch cmd def description
    div epigrafe epigraph fi fig fila h0 h1 h2 h3 h4 h5 h6 hr
    img in include incluye indent item justify left li links
    list meta out outdent p pie put recuadro regla render
    right row rule scope tabla table tags td term th title toc
    tr type
    """.strip().split()
    @TOKEN(s + e + r'\w+')
    def t_SYM(self, t):
        # 'fix_lineno' siempre va antes de cualquier
        # modificación intencional de 't.value'
        t = self.fix_lineno(t)
        t.value = t.value.replace(':', '')
        custom = True if t.value[0] == '_' else False
        if not t.value in self.valid_syms:
            if not t.value[0] == '_':
                self.user_error(t, 'símbolo inválido')
            else:
                t.value = t.value[1:]
                self.user_warn(t, 'símbolo personalizado')
        return t

    # Símbolos para iniciar bloques en modo breve
    valid_shortcuts = {
        # ! Preservar orden para evitar errores de captura
        # Ejemplo: si box está antes de img, img será
        # capturada como recuadro importante por el '!'
        # Es probable que list, img y table tengan que
        # analizarse de manera independiente debido a
        # que su sintaxis también implica contenido o,
        # para el caso de list, rompe con la estructura
        # de sus ítems
        'list': r'(\*|°|◦|\+|o|x|-{1,4}|[1aAiI]\.)[^\S\n]',
        'img': r'!\[', 'table': r'\|',
        'header': r'([#][0-6]|[#]{1,6})','hr': r'={3,}',
        'block_quote': r'>', 'block_code': r'\$>\w*',
        'box': r'(!|\?|(NOTE|WARN|ERROR|TODO):)',
    }
    shortcuts = list(valid_shortcuts.values())
    @TOKEN(s + r'(' + r'|'.join(shortcuts) + r')' + s)
    def t_SHORTMODE(self, t):
        t = self.fix_t(t, self.valid_shortcuts)
        t = self.fix_lineno(t)
        return t

    # Paréntesis de apertura para iniciar argumentos
    @TOKEN(s + r'\(')
    def t_ARGSINI(self, t):
        return self.fix_lineno(t)

    # Paréntesis de cierre para terminar argumentos
    @TOKEN(s + r'\)')
    def t_ARGSEND(self, t):
        return self.fix_lineno(t)

    # Corchete de apertura para iniciar conjuntos
    @TOKEN(s + r'\[')
    def t_ARRAYINI(self, t):
        return self.fix_lineno(t)

    # Corchete de cierre para terminar conjuntos
    @TOKEN(s + r'\]')
    def t_ARRAYEND(self, t):
        return self.fix_lineno(t)

    # Parámetros de los argumentos para los bloques
    @TOKEN(s + r'\w+' + e + s)
    def t_KEY(self, t):
        t = self.fix_lineno(t)
        t.value = t.value.replace(':', '')
        return t

    # Cadenas de texto como valor de parámetro
    @TOKEN(s + str)
    def t_STR(self, t):
        t = self.fix_lineno(t)
        # Para que detecte caracteres escapados
        # cfr. https://stackoverflow.com/a/24242596
        t.value = codecs.decode(
                t.value[1:-1], 'unicode_escape')
        return t

    # Cadenas de texto crudas como valor de parámetro
    @TOKEN(s + raw)
    def t_RAW(self, t):
        t = self.fix_lineno(t)
        t.value = t.value[1:-1]
        return t

    # Número decimal como valor de parámetro
    @TOKEN(s + i0 + r'\.' + i1)
    def t_FLOAT(self, t):
        t = self.fix_lineno(t)
        t.value = float(t.value.replace('_', ''))
        return t

    # Números enteros como valor de parámetro
    @TOKEN(s + i1)
    def t_INT(self, t):
        t = self.fix_lineno(t)
        t.value = int(t.value.replace('_', ''))
        return t

    # Opciones válidas para parámetro
    valid_opts = """
    angle angular arabic ascii_double ascii_single attention
    bar bullet camelCase CamelCase capital caution center
    circle curly danger dash diple double em_dash error false
    guillemet hint important indent init justify left lower
    lower_alpa lower_roman minus none outdent right round
    single smallcaps square task tip todo true upper
    upper_alpha upper_roman warn
    """.strip().split()
    @TOKEN(s + r'(' + r'|'.join(valid_opts) + r')')
    def t_OPT(self, t):
        t = self.fix_lineno(t)
        if t.value == 'true' or t.value == 'false':
            t.type = 'BOOL'
            t.value = t.value == 'true'
        return t

    # Separador entre argumentos como valor de parámetro
    @TOKEN(s + r',')
    def t_SEP(self, t):
        return self.fix_lineno(t)

    # Inicio de grupo de bloques
    @TOKEN(s + r'{')
    def t_GROUPINI(self, t):
        return self.fix_lineno(t)

    # Inicio de grupo de bloques
    @TOKEN(s + r'}')
    def t_GROUPEND(self, t):
        return self.fix_lineno(t)

    # Ignora saltos de línea dobles
    @TOKEN(r'(' + s + r'\n)+')
    def t_CUT(self, t):
        t = self.fix_lineno(t)
        pass # No es necesario

    # Bloque de párrafo o contenido de bloque
    @TOKEN(r'(' + s + r'[^\s}' + e + r'].*\n{0,1})+')
    def t_TXT(self, t):
        # Por el verbatim el 'strip' de este token es False
        return self.fix_lineno(t, strip = False)

    # En caso de error en la obtención de tokens
    def t_error(self, t):
        msg = self.error_msg
        ln = t.lineno
        if not self.error_by_user:
            # Obtiene la línea que no pudo pasar el lexer
            t.value = t.value[0:10].split('\n')[0]
        print(f"ERROR: línea {ln}: {msg}: {t.value}")
        raise

    # En caso de error por parte del usuario
    def user_error(self, t, msg):
        self.error_by_user = True
        self.error_msg = msg
        self.t_error(t)

    # En caso de advertencia por parte del usuario
    def user_warn(self, t, msg):
        print(f"WARN: línea {t.lineno}: {msg}: {t.value}")

    # Imprime el resultado del lexer
    def test(self, data):
        self.lexer.input(data)
        index = 0
        while True:
            t = self.lexer.token()
            index += 1
            if not t:
                break
            print(f"{index} {self.test_msg(t)}")

    # Mensaje para la impresión
    def test_msg(self, t):
        if hasattr(t, 'args'):
            return f"{str(t)[0:-1]},{t.args})"
        else:
            return t

    # Por defecto ply no «sabe nada» acerca de números de
    # línea, así que lo siguiente es para ello
    # Cfr. https://ply.readthedocs.io/en/latest/ply.html#line-numbers-and-positional-information

    # Arregla posición de línea
    def fix_lineno(self, t, strip = True):
        # -1 porque siempre obtiene una línea de más
        t.lexer.lineno += len(t.value.split('\n')) - 1
        t.value = t.value.rstrip()
        if strip:
            t.value = t.value.lstrip()
        return  t

    # No se arregla 'lexpos' (posición de columna) porque
    # el autor de PLY recomienda calcularlo solo cuando
    # hay un error y no para cada token
    # cfr. https://ply.readthedocs.io/en/latest/ply.html#line-numbers-and-positional-information

    # Las siguientes definiciones son para arreglar
    # los tokens cuyo valor fue escrito en short mode
    import re

    # Inicia arreglo de token a través de un diccionario,
    # que tiene como llave la etiqueta (posible) del token
    # y como valor, el regex que usó el lexer para tokenizar
    def fix_t(self, t, dic, type = 'SYM'):
        t.type = type
        for sym in dic:
            if self.re.search(r'^' + dic[sym], t.value):
                old_val = t.value.strip()
                t.value = self.fix_val(sym, old_val)
                t = self.add_args(t, old_val)
                break
        return t

    # Según el valor del token se intentan hacer
    # procesamientos adicionales o se termina de arreglar
    def fix_val(self, sym, val):
        try:
            return eval(f"self.fix_val_{sym}")(val)
        except:
            return sym

    # Arregla los valores de los encabezados
    def fix_val_header(self, val):
        vals = val.split('#')
        if vals[-1]:
            return f"h{vals[-1]}"
        else:
            return f"h{len(vals) - 1}"

    # Las siguientes definiciones son para añadir los
    # argumentos de los tokens escritos en short mode

    # Añade argumento a un token
    def add_arg(self, t, par, val):
        if not hasattr(t, 'args'):
            t.args = {}
        t.args[par] = val
        return t

    # Añade argumentos a un token si los tiene
    def add_args(self, t, val):
        try:
            return eval(f"self.add_args_{t.value}")(t, val)
        except:
            return t

    # Añade argumento de las listas
    list_opts = {
        'bullet': '*', 'circle': '°', 'square': '+',
        'task': 'x', 'dash': '-', 'minus': '--',
        'em_dash': '---', 'bar': '----', 'arabic': '1',
        'lower_alpha': 'a', 'upper_alpha': 'A',
        'lower_roman': 'i', 'upper_roman': 'I',
    }
    def add_args_list(self, t, val):
        name = 'x' if 'o' in val else val
        name = '°' if '◦' in name else name
        name = name.replace('.', '').strip()
        for opt in self.list_opts:
            if name == self.list_opts[opt]:
                t = self.add_arg(t, 'style', opt)
                break
        return t

    # Añade argumento de los recuadros
    def add_args_box(self, t, val):
        name = val.replace(':', '').lower()
        if name == '!':
            name = 'important'
        elif name == '?':
            name = 'tip'
        t = self.add_arg(t, 'type', name)
        return t

    # Añade argumento de los bloques de código
    def add_args_block_code(self, t, val):
        lang = val[2:]
        if not lang:
            lang = 'none'
        t = self.add_arg(t, 'lang', lang)
        return t


if __name__ == '__main__':
    import sys
    ed = open(sys.argv[1], 'r').read()
    lexer = Lexer()
    lexer.test(ed)
